#!/bin/bash

echo "
 ____  _            _   _                 
| __ )| |_   _  ___| |_(_) __ _  ___ _ __ 
|  _ \\| | | | |/ _ \\ __| |/ _\` |/ _ \\ '__|
| |_) | | |_| |  __/ |_| | (_| |  __/ |   
|____/|_|\\__,_|\\___|\\__|_|\\__, |\\___|_|   
                          |___/           "

RES=$CI_PROJECT_TITLE-$CI_JOB_STAGE
if [ -z "$CI_COMMIT_BRANCH" ]; then
    echo "There has been no branch env been set"
else
    RES=$RES-$CI_COMMIT_BRANCH
fi
if [ -z "$CI_BUILD_TAG" ]; then
    echo "No tags set"
else
    RES=$RES:$CI_BUILD_TAG
fi
if [ -z "$RES" ]; then
    echo "No result was set"
    RES="localBuild"
fi
echo $RES

mvn install > artifacts/build/$RES-log.txt
cp target/*.jar artifacts/build/$RES.jar

echo "Logs saved to > ./artifacts/build/$RES-log.txt"
echo "Jar seved to > ./artifacts/build/$RES.jar"