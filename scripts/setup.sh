#!/bin/bash
echo "          _
 ___  ___| |_ _   _ _ __
/ __|/ _ \\ __| | | | '_ \\
\\__ \\  __/ |_| |_| | |_) |
|___/\\___|\\__|\\__,_| .__/
                   |_|
"

DIR="./artifacts/"
if [ -d "$DIR" ]; then
    echo "Removeing old artifacts"
    rm -r artifacts/
fi
echo "Creating artifact maps"
mkdir artifacts
mkdir artifacts/tests
mkdir artifacts/build