#!/bin/bash
echo "
 ____  _            _   _                 
| __ )| |_   _  ___| |_(_) __ _  ___ _ __ 
|  _ \\| | | | |/ _ \\ __| |/ _\` |/ _ \\ '__|
| |_) | | |_| |  __/ |_| | (_| |  __/ |   
|____/|_|\\__,_|\\___|\\__|_|\\__, |\\___|_|   
                          |___/           "

FORCE=0
for i in "$@"; do
    case $i in
        --force)
            FORCE=1
            ;;
        --help)
            echo "
--force
    Forces the tests to be done
--help
    displays this help menu"
            exit
            ;;
    esac
done

if [ $FORCE = 1 ]; then 
    echo " _____              _             
|  ___|__  _ __ ___(_)_ __   __ _ 
| |_ / _ \\| '__/ __| | '_ \\ / _\` |
|  _| (_) | | | (__| | | | | (_| |
|_|  \\___/|_|  \\___|_|_| |_|\\__, |
                            |___/ "
    if [ -d "./target/site/jacoco" ]; then
        rm -r target/site/jacoco
    fi
fi

if [ -d "./target/site/jacoco" ]; then
    echo "Tests already have been done"
else
    mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn clean test jacoco:report
fi

if [ -d "./target/site/jacoco" ]; then
    rm -r artifacts/tests/jacoco
    cp -r target/site/jacoco artifacts/tests
    echo -n "Total covrage "; grep -m1 -Po '(?<=<td class="ctr2">).*?(?=</td>)' target/site/jacoco/index.html | head -n1
else
    echo "Hello"
    exit 1
fi