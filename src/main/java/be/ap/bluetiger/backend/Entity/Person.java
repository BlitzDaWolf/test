package be.ap.bluetiger.backend.Entity;

public class Person {
    private Long id;
    private String name;

    public Person() {
        
    }

    public Person(String Name) {
        this.name = Name;
    }

    public String GetName(){
        return name;
    }
}