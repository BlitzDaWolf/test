package be.ap.bluetiger.backend;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import be.ap.bluetiger.backend.Entity.Person;

@SpringBootTest
public class PersonTest {

    @Test
    public void CanCreate(){
        Assert.notNull(new Person(), "There was a problem during the making of the person constructor");
    }

    @Test
    public void CanCreateWithArguments(){
        Assert.notNull(new Person("Hello"), "There was a problem during the making of the person constructor with arguments");
    }

    @Test
    public void GetName(){
        Person p = new Person("test");
        Assert.notNull(p.GetName(), "There is no name set");
    }

}